# Maker

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Total Downloads][ico-downloads]][link-downloads]
[![Build Status][ico-travis]][link-travis]
[![StyleCI][ico-styleci]][link-styleci]

> Help to create laravel standardized services

## Installation

Via Composer

``` bash
$ composer require dendev/maker
```

## Usage
Create a manager
``` 
php artisan make:manager my // would create MyManager ( Service,Facade,Provider, tests, model, policy )
```

Create a repository 
``` 
php artisan make:repository my // would create MyRepository ( Service,Facade,Provider, tests, model, policy  )
```
Repository come with crud methods ( from ARepositry parent ).   
It's check policy before authorize action

Create a workflow
``` 
php artisan make:workflow my // would create MyWorkflow ( Service,Facade,Provider, tests, model, policy  )
```
Actually juste empty class

## Testing

``` bash
$ composer test
```

## QA

Phpstan
``` bash
phpstan analyse -l 5 src
``` 

Phploc
``` bash
phploc src 
``` 

Phpcpd
``` bash
phpcpd src 
``` 

Phpmd
``` bash
phpmd src html unusedcode > phpmd-unusedcode.html
``` 

## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

## TODO

auto config log file for repository && manager
inform about register policy ( convetion not respected userModel => userModelPolicy but userModel => UserPolicy is better  => manualy register policy in authserviceProvider)
complete before in policy to authorize admin || super amdin ? 

fix model && table name. Actually model as userModel so eloquent search user_models table to save. add in model $table = users; // remove Model && pluralize 

filament model for User or UserModel !? ( not good idea postfix Model ? )

remove postfix model on model

check seeder ( user modelSeeder agains model )

add make:model
