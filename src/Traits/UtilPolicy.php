<?php

namespace Dendev\Maker\Traits;
use App\Models\User;

trait UtilPolicy {
    public function before(User $user, string $ability): bool|null
    {
        if ($user->hasRole('admin')) {
            return true;
        }

        return null;
    }
}
