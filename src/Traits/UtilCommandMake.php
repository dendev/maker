<?php

namespace Dendev\Maker\Traits;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Pluralizer;
use Illuminate\Support\Str;

trait UtilCommandMake
{
    private string $_name_postfix;
    private string $_stub_filename;
    private string $_name;
    private string $_classname;
    private string $_class;
    private string $_namespace;

    private function _make_model(): void
    {
        $this->_name_postfix = 'Model';
        $this->_output_path = 'app/Models/';
        $this->_stub_filename = 'model.structured';

        $this->_execute('Model');

        $this->_stub_custom_values['model_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
        $this->_stub_custom_values['model_classname'] = $this->_classname;
    }

    private function _make_policy(): void
    {
        $this->_name_postfix = 'Policy';
        $this->_output_path = 'app/Policies/';
        $this->_stub_filename = 'policy.with.util';

        $this->_execute('Policy');

        $this->_stub_custom_values['policy_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
        $this->_stub_custom_values['policy_classname'] = $this->_classname;
    }

    private function _make_factory(): void
    {
        $this->_name_postfix = 'Factory';
        $this->_output_path = 'database/factories';
        $this->_stub_filename = 'factory';

        $this->_set_namespace(); // for laravel default factory stub compatibility
        $this->_set_classname(); // for laravel default factory stub compatibility

        $this->_stub_custom_values['factoryNamespace'] = $this->_namespace;
        $this->_stub_custom_values['factory'] = str_replace('Factory', '', $this->_classname);

        $this->_execute('Factory');

        $this->_stub_custom_values['factory_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
        $this->_stub_custom_values['factory_classname'] = $this->_classname;
    }

    private function _make_seeder(): void
    {
        $this->_name_postfix = 'Seeder';
        $this->_output_path = 'database/seeders/';
        $this->_stub_filename = 'seeder.with.util';

        $this->_stub_custom_values['seeder_tablename'] = $this->_set_tablename();

        $this->_execute('Seeder');

        $this->_stub_custom_values['seeder_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
        $this->_stub_custom_values['seeder_classname'] = $this->_classname;
    }

    private function _execute(string $type): void
    {
        $this->info("\n** Working on $type");

        // set basic
        $this->_set_name();
        $this->_set_classname();
        $this->_set_class(); // alias
        $this->_set_namespace();
        $this->_set_output_path();

        /*
        // debug
        dump( $this->_name);
        dump( $this->_classname);
        dump( $this->_output_path);
        dump( $this->_namespace);
        dd('debug');
        */

        // check directory output
        $output_path = $this->_output_path;
        $this->_make_directory(dirname($output_path));

        // get stub
        $stub_path = $this->_get_stub_path($this->_stub_filename);

        // fill stub
        $contents = $this->_get_contents($stub_path);

        $this->info("\n*** Check exist $output_path");
        if (!$this->files->exists($output_path))
        {
            $this->files->put($output_path, $contents);
            $this->info("+++ {$output_path} created\n");
        }
        else
        {
            $this->info("!!! {$output_path} already exists\n");
        }
    }

    private function _get_stub_path(string $stub_filename): string
    {
        $this->info("\n*** Get stub file $stub_filename");
        $stub_path = __DIR__ . "/../../stubs/$stub_filename.stub";
        $this->info("+++ Get $stub_path");

        return $stub_path;
    }

    private function _get_stub_values(): array // TODO more généric !
    {
        $this->info("**** Fill stub file values");

        $this->_stub_custom_values['user_full_namespace'] = Config::get('maker.user_full_namespace');
        $this->_stub_custom_values['user_class'] = Config::get('maker.user_class');
        $this->_stub_custom_values['name'] = $this->_name;
        $this->_stub_custom_values['classname'] = $this->_classname;
        $this->_stub_custom_values['class'] = $this->_class;
        $this->_stub_custom_values['output_path'] = $this->_output_path;
        $this->_stub_custom_values['namespace'] = $this->_namespace;
        $this->_stub_custom_values['identity'] = Str::snake($this->_name) . '_' .  Str::snake($this->_type);
        $this->_stub_custom_values['facade'] = '\\' . ucfirst($this->_name) . ucfirst($this->_type);

        $values_as_string = '';
        foreach( $this->_stub_custom_values as $key => $value )
        {
            $values_as_string .= $key . ': ' . $value . PHP_EOL;
        }
        $values_as_string = substr($values_as_string, 0, -1);

        $this->info("++++ Values: " . PHP_EOL . $values_as_string);

        return $this->_stub_custom_values;
    }

    private function _get_contents($stub_path): mixed
    {
        $this->info("\n*** Fill stub fill");
        $stub_variables = $this->_get_stub_values();

        $contents = file_get_contents($stub_path);

        foreach ($stub_variables as $search => $replace)
        {
            $contents = str_replace('{{' . $search . '}}', $replace, $contents);
            $contents = str_replace('{{ ' . $search . ' }}', $replace, $contents);
        }
        //$this->info("+++ OK filled");

        return $contents;
    }

    private function _make_directory($path): string
    {
        $this->info("\n*** Check directory ${path}'");
        if (!$this->files->isDirectory($path))
        {
            $this->info("+++ ${path} created");
            $this->files->makeDirectory($path, 0777, true, true);
        }
        else
        {
            $this->info("!!! ${path} already exist");
        }

        return $path;
    }

    private function _set_name(): void
    {
        $tmp = $this->_explode_name();

        $name = end($tmp);
        $this->_name = ucwords(Pluralizer::singular($name));
    }

    private function _set_classname(): void
    {
        $this->_classname = ucwords( $this->_name . $this->_name_postfix);
    }

    private function _set_class(): void
    {
        $this->_class = $this->_classname;
    }

    private function _set_namespace(): void
    {
        $tmp = $this->_explode_name();
        array_pop($tmp);

        $namespace = implode('/', $tmp);

        $full_namespace = $this->_output_path . $namespace;
        $full_namespace = ucwords($full_namespace, '/');

        $full_namespace = str_replace('/', '\\', $full_namespace);
        $full_namespace = str_replace('\\\\', '\\', $full_namespace);

        if(str_ends_with($full_namespace, '\\'))
            $full_namespace = substr_replace($full_namespace, '', -1);

        $this->_namespace = $full_namespace;
    }

    private function _set_output_path(): void
    {

        //$output_path = $this->_namespace . '\\' . $this->_classname . '.php';
        $output_path = $this->_output_path. '\\' . $this->_classname . '.php';
        $output_path = str_replace('\\', '/', $output_path);
        $output_path = str_replace('//', '/', $output_path);
        $output_path = lcfirst($output_path);

        $this->_output_path = $output_path;
    }


    private function _set_tablename(): string
    {
        return Str::plural(Str::snake($this->_name));
    }

    protected function _explode_name(): array
    {
        if( str_contains($this->argument('name'), '\\' ) )
            $tmp = explode('\\', $this->argument('name'));
        else
            $tmp = explode('/', $this->argument('name'));

        return $tmp;
    }
}
