<?php

namespace Dendev\Maker\Services\Managers\Core;

use App\Models\User;
use Dendev\Maker\Traits\UtilService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

abstract class AManagerService implements IManagerService
{
    use UtilService;

    protected string $_model_class;

    public function check_access($user, string $action, ?Model $model = null): bool
    {
        $can = false;

        if( $user)
        {
            if( is_null($model) )
                $can = $user->can($action, $this->_model_class);
            else
                $can = $user->can($action, $model);

            if( $can )
            {
                $can = true;
                Log::stack(['manager', 'stack'])->debug("[ARepositoryService::log_access] ARS:la01 : user ( $user->email $user->id) can do '$action' to {$this->_model_class}", [
                    'user_doing_id' => $user->id,
                    'user_doing_email' => $user->email,
                    'model' => $this->_model_class,
                ]);
            }
            else
            {
               Log::stack(['manager', 'stack'])->warning("[ARepositoryService::log_access] ARS:la01 : user ( $user->email $user->id) can't do '$action' to {$this->_model_class}", [
                    'user_doing_id' => $user->id,
                    'user_doing_email' => $user->email,
                    'model' => $this->_model_class,
                ]);
            }
        }
        else
        {
            Log::stack(['manager', 'stack'])->error("[ARepositoryService::log_access] first arg must be an User", [
                'user' => json_encode($user, JSON_PRETTY_PRINT ),
            ]);
        }


        return $can;
    }
}
