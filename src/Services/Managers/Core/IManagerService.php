<?php

namespace Dendev\Maker\Services\Managers\Core;

use Illuminate\Database\Eloquent\Model;

interface IManagerService
{
    public function check_access($user, string $action, ?Model $model = null): bool;
}
