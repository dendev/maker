<?php

namespace Dendev\Maker\Services\Workflows\Core;

use Dendev\Maker\Traits\UtilService;

abstract class AWorkflowService implements IWorkflowService
{
    use UtilService;
    protected string $_model_class;

}
