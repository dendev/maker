<?php

namespace Dendev\Maker\Services\Repositories\Core;

use Dendev\Maker\Traits\UtilService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

abstract class ARepositoryService implements IRepositoryService
{
    use UtilService;
    protected string $_model_class;


    public function check_access($user, string $action, ?Model $model = null): bool
    {
        $can = false;

        if( $user )
        {
            if( is_null($model) )
                $can = $user->can($action, $this->_model_class);
            else
                $can = $user->can($action, $model);

            if( $can )
            {
                $can = true;
                Log::stack(['repository', 'stack'])->debug("[ARepositoryService::log_access] ARS:la01 : user ( {$user->email} {$user->id}) can do '$action' to {$this->_model_class}", []);
            }
            else
            {
               Log::stack(['repository', 'stack'])->warning("[ARepositoryService::log_access] ARS:la01 : user ( {$user->email} {$user->id}) can't do '$action' to {$this->_model_class}", []);
            }
        }
        else
        {
            Log::stack(['repository', 'stack'])->error("[ARepositoryService::log_access] first arg must be an user", [
                'user' => json_encode($user, JSON_PRETTY_PRINT),
            ]);
        }


        return $can;
    }

    public function create(array $datas, $user): false|Model
    {
        $model = false;

        if( $user )
        {
            if( $this->check_access($user, 'create'))
            {
                $model = new $this->_model_class($datas);
                $model->save();
            }
            else
            {
                Log::stack(['repository', 'stack'])->warning("[ARepositoryService::create] ARS:c02 : user ( $user->email $user->id}) can't do 'create' to $this->_model_class", []);
            }
        }
        else
        {
                 Log::stack(['repository', 'stack'])->warning("[ARepositoryService::create] ARS:c01 : first arg must be an user ", [
                     'user' => json_encode($user, JSON_PRETTY_PRINT)
                 ]);
        }

        return $model;
    }

    public function delete(Model $model, $user): bool|Model
    {
        if( $user )
        {
            if ($this->check_access($user, 'delete'))
            {
                $model->delete();
            }
            else
            {
                $model = false;
                Log::stack(['repository', 'stack'])->warning("[ARepositoryService::delete] ARS:d02 : user ( $user->email $user->id) can't do 'create' to $this->_model_class", []);
            }
        }
        else
        {
            $model = false;
            Log::stack(['repository', 'stack'])->warning("[ARepositoryService::delete] ARS:d01 : first arg must be an user", [
                'user' => json_encode($user, JSON_PRETTY_PRINT)
            ]);
        }

        return $model;
    }

    public function find(int|string $id, $user): false|Model
    {
        $found = false;
        if( $user )
        {
            if ($this->check_access($user, 'search'))
            {
                $found = $this->_model_class::find($id);
            }
            else
            {
                Log::stack(['repository', 'stack'])->warning("[ARepositoryService::find] ARS:f02 user ( $user->email $user->id}) can't do 'find' to $this->_model_class", []);
            }
        }
        else
        {
            Log::stack(['repository', 'stack'])->warning("[ARepositoryService::find] ARS:f01 : first arg must be an user", [
                'user' => json_encode($user, JSON_PRETTY_PRINT)
            ]);
        }


        return $found;
    }

    public function find_by_identity(string $identity, $user): false|Model
    {
        $found = false;

        if( $user )
        {
            if( $this->check_access($user, 'search'))
            {
                $found = $this->_model_class::where('identity', $identity)->first();
            }
            else
            {
                Log::stack(['repository', 'stack'])->warning("[ARepositoryService::find_by_identity] ARS:fbi02 user ( $user->email $user->id) can't do 'search' to $this->_model_class", []);
            }
        }
        else
        {
            Log::stack(['repository', 'stack'])->warning("[ARepositoryService::find_by_identity] ARS:fbi01 : user not found so we can't check his permissions", [
                'user_id' => $user
            ]);
        }

        return $found;
    }

    public function get(int|string $id,  $user): false|Model
    {
        return $this->find($id, $user);
    }

    public function get_by_identity(int|string $identity, $user ): false|Model
    {
        return $this->find_by_identity($identity, $user);
    }

    public function get_all($user, ?array $where = []): false|Collection
    {
        $models = false;

        if( $user )
        {
            if( $this->check_access($user, 'search')) // TODO bof create list ?
            {
                $models = $this->_model_class::where($where)->get();
            }
            else
            {
                Log::stack(['repository', 'stack'])->warning("[ARepositoryService::get_all] ARS:fbi02 user ( $user->email $user->id) can't do 'search' to $this->_model_class", []);
            }
        }
        else
        {
            Log::stack(['repository', 'stack'])->warning("[ARepositoryService::get_all] ARS:fbi01 : first arg must be an user", [
                'user' => json_encode($user, JSON_PRETTY_PRINT)
            ]);
        }

        return $models;
    }

    public function update(mixed $id_or_model, array $datas, $user): bool|Model
    {
        $model = false;

        if( $user )
        {
            if( $this->check_access($user, 'update'))
            {
                $model = $this->_instantiate_if_id($id_or_model, $this->_model_class);
                if( $model )
                    $model->update($datas);
                else
                    Log::stack(['repository', 'stack'])->warning("[ARepositoryService::update] ARS:u03 user ( $user->email $user->id) model not found $this->_model_class", []);
            }
            else
            {
                Log::stack(['repository', 'stack'])->warning("[ARepositoryService::update] ARS:u02 user ( $user->email $user->id) can't do 'update' to $this->_model_class", []);
            }
        }
        else
        {
            Log::stack(['repository', 'stack'])->warning("[ARepositoryService::update] ARS:u01 : first arg must be an user", [
                'user' => json_encode($user, JSON_PRETTY_PRINT)
            ]);
        }

        return $model;
    }
}
