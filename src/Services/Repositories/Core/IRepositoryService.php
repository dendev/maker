<?php

namespace Dendev\Maker\Services\Repositories\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface IRepositoryService
{
    public function check_access($user, string $action, ?Model $model = null): bool;

    public function create(array $datas, $user): false|Model;

    public function delete(Model $model, $user): bool|Model;

    public function find(int|string $id, $user): false|Model;

    public function find_by_identity(string $identity, $user): false|Model;

    public function get(int|string $id, $user): false|Model;

    public function get_by_identity(string $identity, $user): false|Model;

    public function get_all($user, ?array $where = []): false|Collection;

    public function update(mixed $id_or_model, array $datas, $user): bool|Model;
}
