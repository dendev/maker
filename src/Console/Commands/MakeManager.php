<?php

namespace Dendev\Maker\Console\Commands;

use Dendev\Maker\Traits\UtilCommandMake;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class MakeManager extends Command
{
    use UtilCommandMake;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:manager {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make and add Manager class';
    protected Filesystem $files;
    private string $_type = 'Manager';
    private string $_name_postfix;
    private string $_output_path;
    private array $_stub_custom_values = [];

    public function __construct(Filesystem $files)
    {
        parent::__construct();
        $this->files = $files;
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->_make_model();
        $this->_make_policy();
        $this->_make_factory();
        $this->_make_seeder();
        $this->_make_service();
        $this->_make_provider();
        $this->_make_facade();
        $this->_make_test();

        $this->_inform();
    }

    private function _make_service(): void
    {
        $this->_name_postfix = 'ManagerService';
        $this->_output_path = 'app/Services/Managers/';
        $this->_stub_filename = 'service.manager';

        $this->_execute('Service');

        $this->_stub_custom_values['service_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
        $this->_stub_custom_values['service_classname'] = $this->_classname;
        $this->_stub_custom_values['service_identity'] = $this->_stub_custom_values['identity'];
    }

    private function _make_provider(): void
    {
        $this->_name_postfix = 'ManagerProvider';
        $this->_output_path = 'app/Providers/Managers/';
        $this->_stub_filename = 'provider.manager';

        $this->_execute('Provider');

        $this->_stub_custom_values['provider_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
    }

    private function _make_facade(): void
    {
        $this->_name_postfix = 'ManagerFacade';
        $this->_output_path = 'app/Facades/Managers/';
        $this->_stub_filename = 'facade.manager';

        $this->_execute('Facade');

        $this->_stub_custom_values['facade_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
    }

    private function _make_test(): void
    {
        $this->_name_postfix = 'ManagerTest';
        $this->_output_path = 'tests/Unit/Manager/';
        $this->_stub_filename = 'test.unit.manager';

        $this->_execute('Test');

        $this->_stub_custom_values['test_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
    }

    private function _inform(): void // TODO in trait
    {
        $this->info("** Info");
        $this->info("*** Edit config/app.php and add");
        $this->info($this->_stub_custom_values['provider_full_namespace'] . '::class,');
        $this->info("'$this->_name" . "Manager' => " . $this->_stub_custom_values['facade_full_namespace'] . '::class,');

        $this->info("*** Edit database/seeders/DatabaseSeeder.php and add");
        $this->info("\$this->call(". $this->_name. $this->_stub_custom_values['seeder_classname'] . "::class)");
        $this->info("\n");
    }
}

// refs :
