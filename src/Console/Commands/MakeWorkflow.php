<?php

namespace Dendev\Maker\Console\Commands;

use Dendev\Maker\Traits\UtilCommandMake;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class MakeWorkflow extends Command
{
    use UtilCommandMake;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:workflow {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make and add Workflow class';
    protected Filesystem $files;
    private string $_type = 'Workflow';
    private string $_name_postfix;
    private string $_output_path;
    private array $_stub_custom_values = [];

    public function __construct(Filesystem $files)
    {
        parent::__construct();
        $this->files = $files;
    }

    /**
     * Execute the console command.
     */
    public function handle(): void
    {
        $this->_make_model();
        $this->_make_policy();
        $this->_make_factory();
        $this->_make_seeder();
        $this->_make_service();
        $this->_make_provider();
        $this->_make_facade();
        $this->_make_test();

        $this->_inform();
    }

    private function _make_service(): void
    {
        $this->_name_postfix = 'WorkflowService';
        $this->_output_path = 'app/Services/Workflows/';
        $this->_stub_filename = 'service.workflow';

        $this->_execute('Service');

        $this->_stub_custom_values['service_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
        $this->_stub_custom_values['service_classname'] = $this->_classname;
        $this->_stub_custom_values['service_identity'] = $this->_stub_custom_values['identity'];
    }

    private function _make_provider(): void
    {
        $this->_name_postfix = 'WorkflowProvider';
        $this->_output_path = 'app/Providers/Workflows/';
        $this->_stub_filename = 'provider.workflow';

        $this->_execute('Provider');

        $this->_stub_custom_values['provider_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
    }

    private function _make_facade(): void
    {
        $this->_name_postfix = 'WorkflowFacade';
        $this->_output_path = 'app/Facades/Workflows/';
        $this->_stub_filename = 'facade.workflow';

        $this->_execute('Facade');

        $this->_stub_custom_values['facade_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
    }

    private function _make_test(): void
    {
        $this->_name_postfix = 'WorkflowTest';
        $this->_output_path = 'tests/Unit/Workflow/';
        $this->_stub_filename = 'test.unit.workflow';

        $this->_execute('Test');

        $this->_stub_custom_values['test_full_namespace'] = $this->_namespace . '\\' . $this->_classname;
    }

    private function _inform(): void // TODO in trait
    {
        $this->info("** Info");
        $this->info("*** Edit config/app.php and add");
        $this->info($this->_stub_custom_values['provider_full_namespace'] . '::class,');
        $this->info("'$this->_name" . "Workflow' => " . $this->_stub_custom_values['facade_full_namespace'] . '::class,');
    }
}

// refs :
