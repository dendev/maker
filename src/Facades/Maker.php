<?php

namespace Dendev\Maker\Facades;

use Illuminate\Support\Facades\Facade;

class Maker extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'maker';
    }
}
