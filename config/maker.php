<?php

return [

   'logging' => [
       'repository' => [
           'driver' => 'daily',
           'path' => storage_path('logs/maker.log'),
           'level' => 'debug',
           'days' => 14,
       ],
   ],

    'user_full_namespace' => 'App\Models\User',
    'user_class' => 'User',
];
